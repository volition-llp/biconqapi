<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRegister extends Model
{
    protected $table = "admin_reg";
    protected $fillable = [
        'name','email','phone_no','username'
    ];
}
