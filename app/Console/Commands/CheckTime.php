<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'time:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get time at particular time interval';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info(\Carbon\Carbon::now());
    }
}
