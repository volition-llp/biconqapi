<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerEmailMobileVerification extends Model
{
    protected $table = "customer_email_mobile_verification";
}
