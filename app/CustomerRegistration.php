<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerRegistration extends Model
{
    protected $table = 'customer_registration';
}
