<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminLogin;
use App\AdminRegister;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AdminAuthController extends Controller
{
    public function signInView()
    {
       
       return view('admin.pages.login');

	}
	
	// public function adminReg()
	// {
	// 	App\AdminRegister::create([
    //         'name' => 'biconq',
    //         'email' => 'biconq@gmail.com',
    //         'phone_no' => 1234569875,
    //         'username' => 'biconq_admin'
    //     ]);

    //     App\AdminLogin::create([
    //         'username' => 'biconq_admin',
    //         'password' => Hash::make('admin123')
    //     ]);
	// }

    public function signInService(Request $request)
    {
    	 $username = $request->input('username');
    	 $password = $request->input('password');

    	 if(empty($username) || empty($password)){

    	 	\Session::flash('message', "You have to fill the Username and Password");
    	 	\Session::flash('alert-class','alert-danger');

            Log::notice("Try to attempt to login without username and password");

    	 	return redirect()->back();

    	 }else{

    	 	   $check_username = AdminLogin::where('username', $username)->first();
    	 	   $admin_name = AdminRegister::where('username', $username)->first();

    	 	   if(!empty($check_username))
    	 	   	{
    	 	   		if(Hash::check($password,$check_username['password'])){

    	 	   			\Session::put('admin_uname',$username);
    	 	   			\Session::put('admin_name',$admin_name['name']);

                        Log::notice("Admin Login ".'- '. $admin_name['name']);

    	 	   			return redirect(route('admin.dashboard'));

    	 	   		}else {

    	 	   			\Session::flash('message', "Wrong Username and Password");
			    	 	\Session::flash('alert-class','alert-danger');

                         Log::warning("Not Valid Info");

			    	 	return redirect()->back();

    	 	   		}

    	 	   	}else{

    	 	   		\Session::flash('message', "Credentials are not valid");
		    	 	\Session::flash('alert-class','alert-danger');

                     Log::warning("Not Valid Info");

		    	 	return redirect()->back();
    	 	   	}

    	 }
    }

    public function dashboardView()
    {
    	 return view('admin.pages.home');
    }

    public function logout()
    {
        \Session::flush('admin_uname');

        return redirect('/');
    }
}
