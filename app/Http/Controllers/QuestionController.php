<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Subjects;
use App\Questions;
use Webpatser\Uuid\Uuid;
use Illuminate\Validation\Rule;

class QuestionController extends Controller
{	

	// Static Functions ------------------------------------------------


	 public static function Subjects()
    {
    	$subjects_data = Subjects::orderBy('created_at', 'desc')->get();

    	return $subjects_data;
    }


    // Static Functions End ------------------------------------------------

     public function addQuestionAnswerView()
    {
       return view('admin.pages.addQuestions');
    }

    public function saveQuestionData(Request $request)
    {
    	 $validator = Validator::make($request->all(), [
    	 		"question" => 'required',
				"op1" => 'required',
				"op2" => 'required',
				"op3" => 'required',
				"op4" => 'required',
				"rightAnswer" => 'required',
				"perQuesMarks" => 'required|numeric',
				"explanation" => '',
				"subject" => 'required',
				"setno" => 'required|numeric',
				// "setno" => ['required','numeric', 
				//     Rule::unique("questions")->where(function ($query) use ($request) {

                //     return $query
                //         ->where('setno',$request->setno)
                //         ->where('subject',$request->subject);
                // })],

    	 ],[
    	 		"op4.required" => "You need to give input of Option 4",
    	 		"op1.required" => "You need to give input of Option 1",
    	 		"op2.required" => "You need to give input of Option 2",
    	 		"op3.required" => "You need to give input of Option 3",
				 "question.required" => "You must write your question",
				 "setno.numeric" => "Set no will be in number format",
				 "setno.unique" => "Already this set no used in the above subject",

    	 ]);


    	 if($validator->fails()){

    	 	 return redirect(route('admin.questions.view'))
			  				->withInput()
			  				->withErrors($validator);
    	 }else{

    	 	 $ques_data = new Questions();
    	 	 $ques_data->uuid = Uuid::generate()->string;
    	 	 $ques_data->question = $request->input('question');
    	 	 $ques_data->op1 = $request->input('op1');
    	 	 $ques_data->op2 = $request->input('op2');
    	 	 $ques_data->op3 = $request->input('op3');
    	 	 $ques_data->op4 = $request->input('op4');
    	 	 $ques_data->rightAnswer = $request->input('rightAnswer');
    	 	 $ques_data->perQuesMarks = $request->input('perQuesMarks');
    	 	 $ques_data->explanation = $request->input('explanation');
			 $ques_data->subject = $request->input('subject');
			 $ques_data->setno = $request->input('setno');			 
    	 	 $ques_data->save();

    	 	\Session::flash('message',"Question added successfully");
			\Session::flash('alert-class','alert-success');

    	 	 return redirect()->back();

    	 }
    }


    public function QuestionsList()
    {
    	$ques_data = Questions::where('delete_status','0')
    						   ->orderBy('created_at', 'desc')->paginate(10);

    	return view('admin.pages.questionAnswerList')
    				->with('ques_data', $ques_data);
    }

    public function updateQuestionData(Request $request)
    {
    		$validator = Validator::make($request->all(), [
    	 		"question" => 'required',
				"op1" => 'required',
				"op2" => 'required',
				"op3" => 'required',
				"op4" => 'required',
				"rightAnswer" => 'required',
				"perQuesMarks" => 'required|numeric',
				"explanation" => '',
				"subject" => 'required',
				"setno" => 'required|numeric'

    	 ],[
    	 		"op4.required" => "You need to give input of Option 4",
    	 		"op1.required" => "You need to give input of Option 1",
    	 		"op2.required" => "You need to give input of Option 2",
    	 		"op3.required" => "You need to give input of Option 3",
				 "question.required" => "You need to give input of Question",
				 "setno.numeric" => "Set no will be in number format",

    	 ]);


    	 if($validator->fails()){

			 \Session::flash('sess',$request->input('uuid'));
    	 	 return redirect(route('questions.list'))
			  				->withInput()
			  				->withErrors($validator);
    	 }else{

    	 	 $ques_data = Questions::where('uuid',$request->input('uuid'))->update([

    	 	 					'question' => $request->input('question'),
								'op1' => $request->input('op1'),
								'op2' => $request->input('op2'),
								'op3' => $request->input('op3'),
								'op4' => $request->input('op4'),
								'rightAnswer' => $request->input('rightAnswer'),
								'perQuesMarks' => $request->input('perQuesMarks'),
								'explanation' => $request->input('explanation'),
								'subject' => $request->input('subject'),
								'setno' => $request->input('setno'),

    	 	 			]);
    	 	
    	 	\Session::flash('message',"Question update successfully");
			\Session::flash('alert-class','alert-success');

    	 	 return redirect()->back();


    	 }
    }

    public function deleteQuestionData(Request $request)
    {
    		$validator = Validator::make($request->all(), [
    	 		"uuid" => 'required|uuid',

    	 ]);


    	 if($validator->fails()){

    	 	 return redirect(route('admin.questions.view'))
			  				->withInput()
			  				->withErrors($validator);
    	 }else{

    	 	  	$ques_data = Questions::where('uuid',$request->input('uuid'))->update([

    	 	 					'delete_status' => '1',

    	 	 			]);
    	 	
    	 	\Session::flash('message',"Question Deleted Successfully");
			\Session::flash('alert-class','alert-success');

    	 	 return redirect()->back();

    	 }
    }

    public function searchFunctionByQuesSubject(Request $request)
    {
    


    	 $ques_data = 	Questions::where('delete_status','0')
    	 						->orwhere('question', 'LIKE', "%".$request->input('q')."%")
   					 			->orWhere('subject', 'LIKE', "%". $request->input('b') . '%')
   					 			->paginate(10);

   		return view('admin.pages.questionAnswerList')
    				->with('ques_data', $ques_data);		 
   			
	}

	public function makePackagesForCustomer()
	{
		
	}
	

}
