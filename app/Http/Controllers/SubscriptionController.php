<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Subscriptions;


class SubscriptionController extends Controller
{
 
// Controller methods for Web
    public function viewSubscriptionPage()
    {
        return view('admin.pages.subscriptionPage');
    }

    public function saveSubscription(Request $request)
    {
        $validator = Validator::make($request->all(),[
                    'package_name' => 'required',
                    'package_price' => 'required|numeric',
                    'no_of_sets' => 'required|integer',
                    'facilities' => 'required',
                    'package_code' =>'required|unique:subscriptions',
        ],[
            'no_of_sets.integer' => " Set number should be in round format",
            'package_code.unique' => "Subscription code should be unique" 
        ]);

        if ($validator->fails()) {
            
            return redirect(route('subscription.view'))
                            ->withInput()
			  				->withErrors($validator);
        }else{

              $subs_data = new Subscriptions();
              $subs_data->package_code = $request->input('package_code');
              $subs_data->package_name = $request->input('package_name');
              $subs_data->package_price = $request->input('package_price');
              $subs_data->no_of_sets = $request->input('no_of_sets');
              $subs_data->facilities = $request->input('facilities');
              $subs_data->save();

              \Session::flash('message',"Subscription Added");
              \Session::flash('alert-class','alert-success');

              return redirect()->back();
        }
    }

    public function listOfSubscriptionPackages()
    {
        $subs_data = Subscriptions::where('status','1')
                                    ->orderBy('created_at', 'desc')  
                                    ->paginate(10);
    
        return view('admin.pages.subscriptionList')
                ->with('subs_data', $subs_data);
    }

    public function updateSubscription(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'package_name' => 'required',
            'package_price' => 'required|numeric',
            'no_of_sets' => 'required|integer',
            'facilities' => 'required',
            // Checking unique of package code except that id value
            'package_code' =>'required|unique:subscriptions,package_code,'.$request->input('id'), 
            
            ],[
                'no_of_sets.integer' => " Set number should be in round format",
                'package_code.unique' => "Subscription code should be unique" 
            ]);

            if ($validator->fails()) {
                \Session::flash('sess',$request->input('id'));
                return redirect(route('subscription.list'))
                                ->withInput()
                                ->withErrors($validator);
            }else{


                $subs_data = Subscriptions::where('id',$request->input('id'))
                                        ->update([
                                            'package_code' => $request->input('package_code'),
                                            'package_name' => $request->input('package_name'),
                                            'no_of_sets' => $request->input('no_of_sets'),
                                            'package_price' => $request->input('package_price'),
                                            'facilities' => $request->input('facilities'),
                                        ]);

                \Session::flash('message',"Updated");
                \Session::flash('alert-class','alert-success');

                return redirect()->back();
            }
    }



// End of web controller methods


// Controller methods for API




// End Controller methods for API

}
