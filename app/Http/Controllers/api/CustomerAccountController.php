<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Controllers\api\VerificationController;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\CustomerRegistration;
use Webpatser\Uuid\Uuid;
use App\CustomerEmailMobileVerification;
use Carbon\Carbon;

class CustomerAccountController extends Controller
{

    //  Account Status 
    // 1 - Account is open with all thing verified
    // 0 - Account is closed with not all verified
    
    public function customerRegistration(Request $request)
    {
        $rules = [
            'name' =>'required',
            'email' =>'required|email',                    
            'phone' =>'required|digits:10',
            'company_name' =>'required',
            'occupation' =>'required',
            'recent_education' =>'required',
            'subscription_code' =>'required'
          ];

            $response = array('response' => '', 'success'=>false);
            $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                
                    return response()->json(['status' => "901",'message' => $validator->messages()]);

                }else{

                   
                    $user_email_checking = CustomerRegistration::where('email',$request->input('email'))
                                                                ->first();
                 
                    $msg = array();

                    if(!empty($user_email_checking)){

                       $msg[] = "This email-id is already registered with us. Kindly Login";
                       return response()->json(['status' => "901",'message' => $msg]);

                    }

                    else{
                        
                        try {
                            
                            $phone= $request->input('phone');
                            $name = $request->input('name');
                            $username = $request->input('email');
                            $email = $request->input('email');
                           
                            // customer data inserting into db
                            $customer_uuid = Uuid::generate()->string;

                            $customer_data = new CustomerRegistration();
                            $customer_data->uuid = $customer_uuid;
                            $customer_data->name = $name;
                            $customer_data->email = $email;
                            $customer_data->company_name = $request->input('company_name');
                            $customer_data->phone = $phone;
                            $customer_data->occupation = $request->input('occupation');
                            $customer_data->recent_education = $request->input('recent_education');
                            $customer_data->account_status = 0;
                            $customer_data->email_verify = 0;
                            $customer_data->phone_verify = 0;
                            $customer_data->subscription_status = 0;
                            $customer_data->subscription_code = $request->input('subscription_code');
                            $customer_data->save();                     
                            
                            $mobile_otp = rand(100000, 999999); // Short code for mobile
                            $email_otp = rand(100000, 999999); // Short code for email


                            $time_now = Carbon::now(); // time of short generate

                            $expire_in_mins = 15;  // Short code valid upto   
                            
                            $msg = "This code will be valid for ".$expire_in_mins." Mins";
                            if($this->verifyMobileNumber($mobile_otp,$phone,$msg) && $this->verifyEmailId($email_otp,$email,$msg,$name,$username)){

                                 // Save short code to DB  
                                 $save_verify_details = new CustomerEmailMobileVerification();
                                 $save_verify_details->cust_uuid = $customer_uuid;
                                 $save_verify_details->mobilecode = $mobile_otp;
                                 $save_verify_details->emailcode = $email_otp;
                                 $save_verify_details->verify_timestamp = $time_now;
                                 $save_verify_details->expire_in = $expire_in_mins;
                                 $save_verify_details->save();
                                
                                $cmsg ="Registration Successfull & Pending for Email and phone Verification";
                                $code = 900;
                                $data = $customer_uuid;

                            }else{
                                $cmsg ="Not a Valid Mail-id and Phone number";
                                $code = 901;
                                $data = null;
                            }
                            
                            
                            Log::info("Registration done, holdin for verification of mobile and email");
                            return response()->json(['status' => $code,'message' => $cmsg,'data'=> $data]);

                        } catch (\Exception $e) {

                            Log::emergency("Exception: ".$e->getMessage());
                            return response()->json(['status' => "901",'message' => "Internal Server Error"]);
                        
                        }


                         
                        
                    }    
                }
    }
// Verify Customer Mobile Number
    public function verifyMobileNumber($otp,$ph,$msg)
    {   
        try{
            $sendsms = new VerificationController();
            $res = $sendsms->sendSMSForMobileVerification($ph,$msg." ".$otp);

            Log::info("Sent sms to this number ".$ph);
            return 1;
        }catch(\Exception $e){
            Log::info("Exception ".$e->getMessage());
            return 0;
        }
         
           
    }

 // Verify Customer Email Address
    public function verifyEmailId($otp,$email,$msg,$name,$username)
    {
        try{
 
                $sendsms = new VerificationController();
                $res = $sendsms->sendMailForEmailVerification($otp,$email,$msg,$name,$username);

                Log::info("Sent email to this email-id ".$email);
                return 1;

        }catch(\Exception $e){
            Log::info("Exception ".$e->getMessage());
            return 0;
        }
       
    }


   

}
