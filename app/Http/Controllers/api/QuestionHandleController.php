<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Subjects;
use App\Questions;
use Webpatser\Uuid\Uuid;

class QuestionHandleController extends Controller
{	

        public function noOfSetsPerSubjects()
        {
                $ques_data = Questions::where('delete_status','0')
                                    ->orderBy('created_at', 'desc')->paginate(10);

                return response()->json(['status' => "900",'message' => $ques_data]);
        }

}