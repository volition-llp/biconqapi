<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Validator;
use App\CustomerRegistration;
use Twilio\Rest\Client;
use App\Mail\VerifyEmail;
use Carbon\Carbon;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\CustomerEmailMobileVerification;
use App\Subscriptions;



class VerificationController extends Controller
{


        public function sendSMSForMobileVerification($ph,$message){
          try
          {
                $recipients = "+91".$ph;

                $account_sid = getenv("TWILIO_SID");
                $auth_token = getenv("TWILIO_AUTH_TOKEN");
                $twilio_number = getenv("TWILIO_NUMBER");

                $client = new Client($account_sid, $auth_token);

                $response = $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);

                
            }
          catch (\Exception $e)
            {
              Log::emergency($e->getMessage());
              
            }



          return  $response;
        }



        public function sendMailForEmailVerification($otp,$email,$message,$name,$username)
        {
           try
           {
          $response =  \Mail::to($email)->send(new VerifyEmail($otp,$username,$message,$name));
         
            }
          catch (\Exception $e)
            {
              Log::emergency($e->getMessage());
              
            }

          return 1;
       
        }


        public function customerMobileEmailVerification(Request $request)
        {
            $mobilecode = $request->input('mobilecode');
            $emailcode = $request->input('emailcode');
            $cust_uuid = $request->input('cust_uuid');  

            $rules = [
              'mobilecode' =>'required',
              'emailcode' =>'required',
              'cust_uuid' =>'required|uuid'
             
            ];
  
              $response = array('response' => '', 'success'=>false);
              $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
            

              return response()->json(['status' => "901",'message' => $validator->messages()]);

            }else{

                  $time_now = Carbon::now();

                  $verification_data = CustomerEmailMobileVerification::where('cust_uuid',$cust_uuid)
                                                                    ->first();
                  $code_ini_time = $verification_data['verify_timestamp'];
                  $expire_in_mins = $verification_data['expire_in'];

                  $diff_in_minutes = $time_now->diffInMinutes($code_ini_time);
                
                  if($diff_in_minutes < $expire_in_mins){

                        $verification_data = CustomerEmailMobileVerification::where('cust_uuid',$cust_uuid)
                                                                            ->first();
                        if($emailcode == $verification_data['emailcode'] && $mobilecode == $verification_data['mobilecode']){

                          // Code compare if-else block

                          $customer_status_update = CustomerRegistration::where('uuid',$cust_uuid)
                                                                    ->update([
                                                                      
                                                                      'account_status' => 1,
                                                                      'email_verify' => 1,
                                                                      'phone_verify' => 1,
                                                                    ]);
                                                                      
                          $customer_subscription_data = CustomerRegistration::where('uuid',$cust_uuid)
                                                                              ->first();

                          // getting the subscription plan for the particular customer

                          $subscription_code = $customer_subscription_data['subscription_code'];
                          
                          
                          //  Subscription with 1 status is active and 0 status is deactive
                          $subscription_data = Subscriptions::where('package_code',$subscription_code)
                                                              ->where('status',1)
                                                              ->first();

                   
                          return response()->json(['status' => "900",
                                                   'message' => "Email and Mobile both are verified",
                                                   'data'=>$subscription_data]);


                        }else{
                           // if-else code is not matching 
                              
                           response()->json(['status' => "901",'message' => "Kindly check the code again"]);

                        }

                  }else{

                    // Expire time if-else 

                     $del_verification_process = CustomerEmailMobileVerification::where('cust_uuid',$cust_uuid)
                                                                  ->delete();

                    // user has to register from start 
                    // meaning no verification is performed
                    
                    $customer_ac_status_update = CustomerRegistration::where('uuid',$cust_uuid)
                                                                  ->delete();
                     return response()->json(['status' => "901",
                                              'message' => "Code is expired"]);
          
                  }
                  
                                                               


             }
         }

        
}