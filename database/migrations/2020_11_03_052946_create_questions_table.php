<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('question',255);
                $table->string('op1',150);
                $table->string('op2',150);
                $table->string('op3',150);
                $table->string('op4',150);
                $table->string('rightAnswer',100);
                $table->integer('perQuesMarks')->length(50);
                $table->string('explanation',200)->nullable();
                $table->boolean('delete_status')->default(0);
                $table->string('subject',100);
                $table->string('setno',40);
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
