<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_registration', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('name',255);
                $table->string('email',150);
                $table->string('company_name',150);
                $table->string('phone',150);
                $table->string('occupation',150);
                $table->string('recent_education',150);
                $table->string('account_status',100);
                $table->boolean('email_verify')->default(0);
                $table->boolean('phone_verify')->default(0);
                $table->string('subscription_status',150);
                $table->string('subscription_code',150); 
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_registration');
    }
}
