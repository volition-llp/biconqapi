<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerEmailMobileVerificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_email_mobile_verification', function (Blueprint $table) {
            $table->id();
            $table->uuid('cust_uuid');
            $table->string('mobilecode',50);
            $table->string('emailcode',50);
            $table->string('verify_timestamp',100);
            $table->string('expire_in',50);
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_email_mobile_verification');
    }
}
