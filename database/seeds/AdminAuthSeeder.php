<?php

use Illuminate\Database\Seeder;

class AdminAuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\AdminRegister::create([
            'name' => 'biconq',
            'email' => 'biconq@gmail.com',
            'phone_no' => 1234569875,
            'username' => 'biconq_admin'
        ]);

        App\AdminLogin::create([
            'username' => 'biconq_admin',
            'password' => Hash::make('admin123')
        ]);
    }
}
