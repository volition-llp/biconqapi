<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bicon-Q | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
   @include('common.headerlinks')

</head>
<body class="hold-transition register-page">

	  @yield('content')


   @include('common.footerlinks')
</body>
</html>