@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Questions</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{route('questions.list')}}" type="button" class="btn btn-warning btn-sm" >View All</a>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">MCQ Questions</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('questions.save')}}" method="POST">
              	{{csrf_field()}}
                <div class="card-body">
                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Subject<span style="color:red;">*</span></label>
                        <select name="subject" class="form-control" required>
                    @php $sub_data = App\Http\Controllers\QuestionController::Subjects() @endphp
                        @foreach($sub_data as $subject)
                          <option value="{{$subject->book_code}}">{{$subject->book_name}} - ({{$subject->book_code}})</option>
                        @endforeach
                        </select>

                           @if ($errors->has('subject'))
                          <span class="text-danger">{{ $errors->first('subject') }}</span>
                      @endif
                  </div>
                  </div>
                </div>
                
                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Set No.<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="setno" placeholder="Enter Set number..." value="{{old('setno')}}" required>

                        @if ($errors->has('setno'))
                            <span class="text-danger">{{ $errors->first('setno') }}</span>
                        @endif

                  </div>
                  </div>
                </div>

                   <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Question<span style="color:red;">*</span></label>
                    <textarea class="form-control" rows="3" name="question" placeholder="Enter Question..." required>{{old('question')}}</textarea>

                     @if ($errors->has('question'))
                          <span class="text-danger">{{ $errors->first('question') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>

                 <div class="row">
                 	<div class="col-md-12">
                 		  <div class="form-group">
                    <label for="exampleInputEmail1">Option 1<span style="color:red;">*</span></label>
                    <textarea class="form-control" rows="2" name="op1" placeholder="Enter Answer options..." required>{{old('op1')}}</textarea>

                            @if ($errors->has('op1'))
			                    <span class="text-danger">{{ $errors->first('op1') }}</span>
			                @endif

                  </div>
                 	</div>
             
      
                 </div>

                 
                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                    <label for="exampleInputEmail1">Option 2<span style="color:red;">*</span></label>
                    <textarea class="form-control" rows="2" name="op2" placeholder="Enter Answer options..." required>{{old('op2')}}</textarea>

                            @if ($errors->has('op2'))
                          <span class="text-danger">{{ $errors->first('op2') }}</span>
                      @endif

                  </div>
                  </div>
             

                 </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                    <label for="exampleInputEmail1">Option 3<span style="color:red;">*</span></label>
                    <textarea class="form-control" rows="2" name="op3" placeholder="Enter Answer options..." required>{{old('op3')}}</textarea>

                            @if ($errors->has('op3'))
                          <span class="text-danger">{{ $errors->first('op3') }}</span>
                      @endif

                  </div>
                  </div>
             

                 </div>


                 <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Option 4<span style="color:red;">*</span></label>
                    <textarea class="form-control" rows="2" name="op4" placeholder="Enter Answer options..." required>{{old('op4')}}</textarea>

                            @if ($errors->has('op4'))
                          <span class="text-danger">{{ $errors->first('op4') }}</span>
                      @endif

                  </div>
                  </div>
             

                 </div>

                  <div class="row">
                  <div class="col-md-6">
                   <div class="form-group">
                        <label>Choose Right Answer<span style="color:red;">*</span></label>
                        <select name="rightAnswer" class="form-control" required>
                          <option value="op1">Option 1</option>
                          <option value="op2">Option 2</option>
                          <option value="op3">Option 3</option>
                          <option value="op4">Option 4</option>
                        </select>
                          @if ($errors->has('rightAnswer'))
                          <span class="text-danger">{{ $errors->first('rightAnswer') }}</span>
                      @endif

                      </div>
                  </div>
                  
                  <div class="col-md-6">
                   <div class="form-group">
                        <label>Marks (Per question)<span style="color:red;">*</span></label>
                        <select name="perQuesMarks" class="form-control" required>
                          @for($i=1; $i<=10; $i++)
                          <option value="{{$i}}">{{$i}}</option>
                          @endfor
                        </select>
                         @if ($errors->has('perQuesMarks'))
                          <span class="text-danger">{{ $errors->first('perQuesMarks') }}</span>
                      @endif
                      </div>
                  </div>

                 </div>

                   


                 <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Explanation</label>
                    <textarea class="form-control" rows="2" name="explanation" placeholder="Explanation ...">{{old('explanation')}}</textarea>

                            @if ($errors->has('explanation'))
                          <span class="text-danger">{{ $errors->first('explanation') }}</span>
                      @endif

                      
                  </div>
                  </div>
                </div>
                <!-- /.card-body -->
       

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



            <!-- Input addon -->
           
            <!-- Horizontal Form -->
           
          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection