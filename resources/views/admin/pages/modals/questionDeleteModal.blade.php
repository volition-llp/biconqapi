<div class="modal fade" id="ques-del{{$quesData->uuid}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{route('question.delete')}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="uuid" value="{{$quesData->uuid}}">
                 <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <h4 for="exampleInputPassword1">
                      Are you sure you want to delete this question?
                    </h4>
                   

                  </div>
                  </div>

                 </div>
  
               
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <button type="submit" class="btn btn-secondary">Yes</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->