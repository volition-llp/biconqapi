<div class="modal fade" id="ques-edit{{$quesData->uuid}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Edit Question details</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{route('question.update')}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="uuid" value="{{$quesData->uuid}}">

                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Set No.<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="setno" placeholder="Enter Set number..." value="{{$quesData->setno}}" required>

                        @if ($errors->has('setno'))
                            <span class="text-danger">{{ $errors->first('setno') }}</span>
                        @endif

                  </div>
                  </div>
                </div>                
      
                <div class="row">
                  <div class="col-md-12">
                        
                      <div class="form-group">
                        <label>Subject<span style="color:red;">*</span></label>
                        <select name="subject" class="form-control" required>
                @php $sub_data = App\Http\Controllers\QuestionController::Subjects() @endphp
                        
                          @foreach($sub_data as $sub)
                               @if($quesData->subject == $sub->book_code)
                                <option value="{{$sub->book_code}}" selected>{{$sub->book_name}} - {{$sub->book_code}}</option>
                               @else
                                <option value="{{$sub->book_code}}">{{$sub->book_name}} - {{$sub->book_code}}</option>
                               @endif
                              @endforeach
                    

                        </select>
                      
                           @if ($errors->has('subject'))
                          <span class="text-danger">{{ $errors->first('subject') }}</span>
                      @endif
                  </div>
                  </div>

                 </div>



                 <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Question<span style="color:red;">*</span> </label>
                    <textarea class="form-control" rows="3" name="question" placeholder="Write your question ..." required>{{$quesData->question}}</textarea>

                     @if ($errors->has('question'))
                          <span class="text-danger">{{ $errors->first('question') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>

             
                  <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Option 1<span style="color:red;">*</span> </label>
                    <textarea class="form-control" rows="3" name="op1" placeholder="Write your option 1 ..." required>{{$quesData->op1}}</textarea>

                     @if ($errors->has('op1'))
                          <span class="text-danger">{{ $errors->first('op1') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>

              <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Option 2<span style="color:red;">*</span> </label>
                    <textarea class="form-control" rows="3" name="op2" placeholder="Write your option 2 ..." required>{{$quesData->op2}}</textarea>

                     @if ($errors->has('op2'))
                          <span class="text-danger">{{ $errors->first('op2') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>

                <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Option 3<span style="color:red;">*</span> </label>
                    <textarea class="form-control" rows="3" name="op3" placeholder="Write your option 3 ..." required>{{$quesData->op3}}</textarea>

                     @if ($errors->has('op3'))
                          <span class="text-danger">{{ $errors->first('op3') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>


                  <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Option 4<span style="color:red;">*</span> </label>
                    <textarea class="form-control" rows="3" name="op4" placeholder="Write your option 4 ..." required>{{$quesData->op4}}</textarea>

                     @if ($errors->has('op4'))
                          <span class="text-danger">{{ $errors->first('op4') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>

                <div class="row">
                  <div class="col-md-12">
                        
                      <div class="form-group">
                        <label>Choose Right Answer<span style="color:red;">*</span></label>
                        <select name="rightAnswer" class="form-control" required>

                         @if($quesData->rightAnswer == "op1")
                            <option value="op1" selected>Option 1</option>
                            <option value="op2">Option 2</option>
                            <option value="op3">Option 3</option>
                            <option value="op4">Option 4</option>
                          @elseif($quesData->rightAnswer == "op2")
                            <option value="op1" >Option 1</option>
                            <option value="op2" selected>Option 2</option>
                            <option value="op3">Option 3</option>
                            <option value="op4">Option 4</option>
                          @elseif($quesData->rightAnswer == "op3")
                            <option value="op1" >Option 1</option>
                            <option value="op2">Option 2</option>
                            <option value="op3" selected>Option 3</option>
                            <option value="op4">Option 4</option>
                          @elseif($quesData->rightAnswer == "op4")
                            <option value="op1" >Option 1</option>
                            <option value="op2">Option 2</option>
                            <option value="op3">Option 3</option>
                            <option value="op4" selected>Option 4</option>
                          @else
                            <option value="op1" >Option 1</option>
                            <option value="op2">Option 2</option>
                            <option value="op3">Option 3</option>
                            <option value="op4">Option 4</option>
                          @endif

                        </select>
                    
                           @if ($errors->has('rightAnswer'))
                          <span class="text-danger">{{ $errors->first('rightAnswer') }}</span>
                      @endif
                  </div>
                  </div>

                 </div>



                 <div class="row">
                  <div class="col-md-12">
                        
                      <div class="form-group">
                        <label>Explanation<span style="color:red;">*</span></label>
                        <textarea class="form-control" rows="3" name="explanation" placeholder="Write your explanation...">{{$quesData->explanation}}</textarea>

                     @if ($errors->has('explanation'))
                          <span class="text-danger">{{ $errors->first('explanation') }}</span>
                      @endif
                    
                  </div>
                  </div>

                 </div>


                <div class="row">
                  <div class="col-md-12">
                        
                      <div class="form-group">
                        <label>Marks<span style="color:red;">*</span></label>
                         <select name="perQuesMarks" class="form-control" required>
                          @for($i=1; $i<=10; $i++)
                              @if($quesData->perQuesMarks == $i)
                                <option value="{{$i}}" selected>{{$i}}</option>
                               @else
                                <option value="{{$i}}">{{$i}}</option>
                               @endif
                          @endfor
                        </select>
                      @if ($errors->has('perQuesMarks'))
                          <span class="text-danger">{{ $errors->first('perQuesMarks') }}</span>
                      @endif
                  </div>
                  </div>

                 </div>

  
               
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Save changes</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->