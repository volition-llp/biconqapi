<div class="modal fade" id="subs-edit{{$subsdata->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Edit Question details</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{route('subscription.update')}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$subsdata->id}}">

                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Package Code<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="package_code" placeholder="Enter Package Code..." value="{{$subsdata->package_code}}" required>

                        @if ($errors->has('package_code'))
                            <span class="text-danger">{{ $errors->first('package_code') }}</span>
                        @endif

                  </div>
                  </div>
                </div>                
      
                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Package Name<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="package_name" placeholder="Enter Package Name..." value="{{$subsdata->package_name}}" required>

                        @if ($errors->has('package_name'))
                            <span class="text-danger">{{ $errors->first('package_name') }}</span>
                        @endif

                  </div>
                  </div>
                </div>   

                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>No of Sets<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="no_of_sets" placeholder="Enter Numbers of Sets..." value="{{$subsdata->no_of_sets}}" required>

                        @if ($errors->has('no_of_sets'))
                            <span class="text-danger">{{ $errors->first('no_of_sets') }}</span>
                        @endif

                  </div>
                  </div>
                </div>   

                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Package Price<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="package_price" placeholder="Enter Package Price..." value="{{$subsdata->package_price}}" required>

                        @if ($errors->has('package_price'))
                            <span class="text-danger">{{ $errors->first('package_price') }}</span>
                        @endif

                  </div>
                  </div>
                </div>   

             
      


                 <div class="row">
                  <div class="col-md-12">
                        
                      <div class="form-group">
                        <label>Facilities<span style="color:red;">*</span></label>
                        <textarea class="form-control" rows="3" name="facilities" placeholder="Facilities...">{{$subsdata->facilities}}</textarea>

                     @if ($errors->has('facilities'))
                          <span class="text-danger">{{ $errors->first('facilities') }}</span>
                      @endif
                    
                  </div>
                  </div>

                 </div>


  
               
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Save changes</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->