@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Questions</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('admin.questions.view')}}" type="button" class="btn btn-warning btn-sm" >Add Questions</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            
            
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
      <div class="container-fluid">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">MCQ Questions</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('questions.search')}}" method="get">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                      <div class="col-md-6">
                    <input type="text" id="ques" class="form-control" name="q" placeholder="Write Question name...." value="">
                  </div>
                  <div class="col-md-6">
                     <input type="text" id="sub" class="form-control" name="b" placeholder="Write Book name ...." value="">
                  </div>
                </div><br>
                <div class="row">
                  <div class="col-md-9">
                  
                     <button type="submit" class="btn btn-primary">Search</button>

               
                </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    </section>    
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Questions List</h3>
              </div>
              <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                <table class="table table-hover" id="tab">
                  <thead>                  
                    <tr>
                      <th>Set#</th>
                      <th>Subject</th>
                      <th>Question</th>
                      <th>Option 1</th>
                      <th>Option 2</th>
                      <th>Option 3</th>
                      <th>Option 4</th>
                      <th>Right Answer Option</th>
                      <th>Explanation</th>
                      <th>Marks</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @if(empty($ques_data))

                          <tr>
                          <td></td>
                           <td></td>
                           <td></td>
                            <td></td>
                            <td>No Data</td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                          </tr>
                
                    @else
 @php $sub_data = App\Http\Controllers\QuestionController::Subjects() @endphp
                             @foreach($ques_data as $dt)
                          <tr>
                            <td>{{ $dt->setno }} </td>
                            
                            @foreach($sub_data as $sub)
                                @if($dt->subject == $sub->book_code)
                                <td>{{ $sub->book_name }}</td>
                                @break
                                @endif
                              @endforeach
                              
                            <td>{{ $dt->question }} </td>
                            <td>{{ $dt->op1 }}</td>
                            <td>{{ $dt->op2 }}</td>
                            <td>{{ $dt->op3 }}</td>
                            <td>{{ $dt->op4 }}</td>
                            <td>{{ $dt->rightAnswer }}</td>
                            <td>{{ $dt->explanation }}</td>
                            <td>{{ $dt->perQuesMarks }}</td>
                            <td>
                               <a href="" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#ques-edit{{$dt->uuid}}" >Edit</a>
                              @include('admin.pages.modals.questionEditModal',['quesData'=> $dt])
                              <br><br>
                              <a href="" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ques-del{{$dt->uuid}}" >Delete</a>

                               @include('admin.pages.modals.questionDeleteModal',['quesData'=> $dt])

                            </td>
                            
                          </tr>

                          
                         
                       @endforeach
                    @endif
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                   {{ $ques_data->render("pagination::bootstrap-4") }}
                </ul>
              </div>
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
        
        </div>
        <!-- /.row -->
    
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->




  </div>
  <!-- /.content-wrapper -->
  
                            

@endsection

        <!-- Opening modal for error in edit section -->
                                  
        @if (count($errors) > 0)
                              <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                              <script>
                              $(document).ready(function(){
                                   
                                     $('#ques-edit{{Session::get("sess")}}').modal('show'); 

                              });
                              @endif

                                  

                            </script>