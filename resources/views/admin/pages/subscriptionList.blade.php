@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Subscription</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('subscription.view')}}" type="button" class="btn btn-warning btn-sm" >Add Subscription</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            
            
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
      <div class="container-fluid">

    </section>    
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Packages</h3>
              </div>
              <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                <table class="table table-hover" id="tab">
                  <thead>                  
                    <tr>
                      <th>Package Code#</th>
                      <th>Package Name</th>
                      <th>Number of Sets</th>
                      <th>Package Price</th>
                      <th>Facilities</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @if(empty($subs_data))

                          <tr>
                          <td></td>
                           <td></td>
                           <td></td>
                            <td></td>
                            <td>No Data</td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                          </tr>
                
                    @else
                             @foreach($subs_data as $dt)
                          <tr>
                            <td>{{ $dt->package_code }} </td>
                            <td>{{ $dt->package_name }} </td>
                            <td>{{ $dt->no_of_sets }}</td>
                            <td>{{ $dt->package_price }}</td>
                            <td>{{ $dt->facilities }}</td>
               
                            <td>
                               <a href="" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#subs-edit{{$dt->id}}" >Edit</a>
                              @include('admin.pages.modals.subscriptionEditModal',['subsdata'=> $dt])
                              <br><br>
                              <a href="" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#subs-del{{$dt->id}}" >Delete</a>

                           

                            </td>
                            
                          </tr>

                          
                         
                       @endforeach
                    @endif
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                   {{ $subs_data->render("pagination::bootstrap-4") }}
                </ul>
              </div>
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
        
        </div>
        <!-- /.row -->
    
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->




  </div>
  <!-- /.content-wrapper -->
  
                            

@endsection

        <!-- Opening modal for error in edit section -->
                                  
        @if (count($errors) > 0)
                              <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                              <script>
                              $(document).ready(function(){
                                   
                                     $('#subs-edit{{Session::get("sess")}}').modal('show'); 

                              });
                              @endif

                                  

                            </script>