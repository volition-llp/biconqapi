@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Subscription Package</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{route('subscription.list')}}" type="button" class="btn btn-warning btn-sm" >View All</a>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Packages</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('subscription.save')}}" method="POST">
              	{{csrf_field()}}
                <div class="card-body">
           
                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Package Code<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="package_code" value="{{old('package_code')}}"  placeholder="Enter Package Code..." required>

                        @if ($errors->has('package_code'))
                            <span class="text-danger">{{ $errors->first('package_code') }}</span>
                        @endif

                  </div>
                  </div>
                </div>


                <div class="row">
                     <div class="col-md-12">
                    <div class="form-group">
                    <label>Package Name<span style="color:red;">*</span></label>
                   <input type="text" class="form-control" name="package_name" value="{{old('package_name')}}"  placeholder="Enter Package Name..." required>

                        @if ($errors->has('package_name'))
                            <span class="text-danger">{{ $errors->first('package_name') }}</span>
                        @endif

                  </div>
                  </div>
                </div>

                   <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Price<span style="color:red;">*</span></label>
                    <input type="text" class="form-control" name="package_price" value="{{old('package_price')}}"  placeholder="Enter Price..." required>

                     @if ($errors->has('package_price'))
                          <span class="text-danger">{{ $errors->first('package_price') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>



                 <div class="row">
                 	<div class="col-md-12">
                 		  <div class="form-group">
                    <label for="exampleInputEmail1">No. of Sets (In a Package)<span style="color:red;">*</span></label>
                    <input type="text" class="form-control" name="no_of_sets" value="{{old('no_of_sets')}}"  placeholder="Enter No. of Sets..." required>

                            @if ($errors->has('no_of_sets'))
			                    <span class="text-danger">{{ $errors->first('no_of_sets') }}</span>
			                @endif

                  </div>
                 	</div>
             
      
                 </div>

                 
                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                    <label for="exampleInputEmail1">Facilities<span style="color:red;">*</span></label>
                      <textarea rows=3 class="form-control" name="facilities"  placeholder="Enter the facilities of this package..." required>{{old('facilities')}}</textarea>

                            @if ($errors->has('facilities'))
                          <span class="text-danger">{{ $errors->first('facilities') }}</span>
                      @endif

                  </div>
                  </div>
             

                 </div>

                 
                  

                 
                <!-- /.card-body -->
       

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



            <!-- Input addon -->
           
            <!-- Horizontal Form -->
           
          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection