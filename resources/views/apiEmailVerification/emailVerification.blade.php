<!DOCTYPE html>
<html>
  <head>
    <title>Welcome Email</title>
  </head>
  <body>
    <h2>Welcome to the site {{$username}}</h2>
    <br/>
    Your registered email-id is {{$name}} , Verification Code
    <br/>
    {{$otp}}
  </br>

  <a href="/verification/{{$username}}">Verification Link</a>
  </body>
</html>