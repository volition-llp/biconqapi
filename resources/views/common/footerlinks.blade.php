<!-- jQuery -->
<script src="/asset/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/asset/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/asset/dfiles/js/adminlte.min.js"></script>


<!-- REQUIRED SCRIPTS -->
<!-- overlayScrollbars -->
<script src="/asset/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="/asset/dfiles/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="/asset/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="/asset/plugins/raphael/raphael.min.js"></script>
<script src="/asset/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="/asset/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="/asset/plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="/asset/dfiles/js/pages/dashboard2.js"></script>

<!-- bs-custom-file-input -->
<script src="/asset/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="/asset/dfiles/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/asset/dfiles/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
 
<!-- Select2 -->
<script src="/asset/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="/asset/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="/asset/plugins/moment/moment.min.js"></script>
<script src="/asset/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="/asset/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="/asset/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/asset/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="/asset/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
