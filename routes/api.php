<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'],function(){

Route::prefix('v1/client/')->group(function () {
         
        // Customer registration
        Route::post('signup/', "api\CustomerAccountController@customerRegistration");

        // Verify the email address with verification db
        Route::post('verification/', "api\VerificationController@customerMobileEmailVerification");

        // create payment for customer
        Route::post('payment', "api\PaymentController@payNow");


        // Set wise question list with pagination
        Route::get('questions', "api\QuestionHandleController@noOfSetsPerSubjects");
    });

});