<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','AdminAuthController@signInView');

Route::get('/admin','AdminAuthController@signInView');

Route::post('/auth/service','AdminAuthController@signInService')
    			->name('auth.admin');

Route::group(['middleware' => 'authCheckAdmin'],function(){   //Session validation in every pages

Route::prefix('admin')->group(function () {

    Route::get('/dashboard','AdminAuthController@dashboardView')
    			->name('admin.dashboard');

     // Adding Questions Answer

    Route::get('/questions','QuestionController@addQuestionAnswerView')
    		    ->name('admin.questions.view');

    Route::post('/questions/save','QuestionController@saveQuestionData')
    			->name('questions.save');

    Route::get('/questions/list','QuestionController@QuestionsList')
    			->name('questions.list');

    Route::get('/questions/search','QuestionController@searchFunctionByQuesSubject')
    			->name('questions.search');

    Route::post('/questions/delete','QuestionController@deleteQuestionData')
    			->name('question.delete');

    Route::post('/questions/update','QuestionController@updateQuestionData')
                ->name('question.update');
                

    // Subscription Adding
    Route::get('/subscription', 'SubscriptionController@viewSubscriptionPage')
                ->name("subscription.view");
    Route::post('/subscription/save','SubscriptionController@saveSubscription')
                ->name('subscription.save');
    Route::get('/subscription/list','SubscriptionController@listOfSubscriptionPackages')
                ->name('subscription.list');
    Route::post('/subscription/update','SubscriptionController@updateSubscription')
                ->name('subscription.update');
    // Logout 
    Route::get('/logout','AdminAuthController@logout')
    			->name('admin.logout');
});


});
 
//  Admin Route Ends
